---
title: 'নিজেকে রক্ষা করতে এবং রোগের বিস্তার রোধ করতে আমি কী করতে পারি?'
taxonomy:
    category:
        - COVID-19
        - CORONA
    tag:
        - 'COVID-19 protect myself'
        - 'COVID-19 protection'
child_type: docs
---

রোগের বিস্তার রোধে এবং নিজেকে রক্ষার জন্য এই পর্বকে দু'টি ভাগে ভাগ করা হয়েছে যেমনঃ

1. [প্রত্যেকের জন্য সুরক্ষা ব্যবস্থা](https://al-amin.info/doc/covid-19/protect-myself/protection-measures-for-everyone) এবং 
2. [যে সকল ব্যক্তি COVID-19 ছড়িয়ে পড়ছে এমন যায়গায় সম্প্রতি বা গত ১৪ দিন পরিদর্শন করেছেন এমন ব্যক্তিদের সুরক্ষা ব্যবস্থা](https://al-amin.info/doc/covid-19/protect-myself/covid-19-spreading)