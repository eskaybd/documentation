---
title: 'এমন কিছু আছে যা আমার করা উচিত নয়?'
taxonomy:
    category:
        - docs
---

নিম্নলিখিত পদক্ষেপগুলি COVID-2019 এর বিরুদ্ধে কার্যকর নয় এবং ক্ষতিকারক হতে পারেঃ

    ১) ধূমপান
    ২) একাধিক মাস্ক পরা
    ৩) অ্যান্টিবায়োটিক গ্রহণ (প্রশ্ন 10 দেখুন "এমন কোন ওষুধ বা থেরাপি রয়েছে যা COVID-19 প্রতিরোধ বা নিরাময় করতে পারে ?")

যে কোনও ক্ষেত্রে, যদি আপনার জ্বর, কাশি এবং শ্বাস নিতে সমস্যা হয় তবে আরও গুরুতর সংক্রমণ হওয়ার ঝুঁকি কমাতে প্রাথমিকভাবে চিকিৎসক এর শরণাপন্ন হওয়া  এবং আপনার চিকিৎসক এর  সাথে আপনার সাম্প্রতিক ভ্রমণ ইতিহাস শেয়ার করা ।