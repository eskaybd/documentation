---
title: 'COVID-19-এর সময়ে আমি কীভাবে নিরাপদে মুদি দোকানে বা বাজারে যেতে পারি?'
taxonomy:
    category:
        - docs
---

মুদি দোকানে যাওয়ার সময়, অন্যের থেকে কমপক্ষে 1-মিটার দূরত্ব বজায় রাখুন এবং আপনার চোখ, মুখ এবং নাক স্পর্শ করা এড়িয়ে চলুন।  সম্ভব হলে শপিংয়ের আগে শপিং ট্রলির বা ঝুড়িগুলির হ্যান্ডলগুলি স্যানিটাইজ করুন।  বাড়িতে একবার, আপনার ক্রয় করা পণ্যগুলি পরিবহন ও সঞ্চয় করার পরে আপনার হাত ভালভাবে ধুয়ে ফেলুন।  খাদ্য বা খাদ্য প্যাকেজিংয়ের মাধ্যমে COVID-19 সংক্রমনের কোনও নিশ্চিত ঘটনা এখনও নেই।
