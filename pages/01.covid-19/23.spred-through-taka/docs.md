---
title: 'মুদ্রা এবং নোটের মাধ্যমে COVID-19 কী ছড়িয়ে যেতে পারে?'
metadata:
    'ব্যাংক নোট, টাকা ': টাকা
taxonomy:
    category:
        - COVID-19
        - CORONA
        - TAKA
    tag:
        - Taka
        - 'bank note'
        - coin
---

COVID-19 ভাইরাসটি কয়েন বা নোটের মাধ্যমে সংক্রামিত হতে পারে তা নিশ্চিত বা অস্বীকার করার কোনও প্রমাণ বর্তমানে নেই।  তবে, সংক্রামিত ব্যক্তির থেকে বের হওয়া শ্বাস প্রশ্বাসের  তরল ফোঁটাগুলি দূষিত হতে পারে এবং বিভিন্ন  পৃষ্ঠের উপরে পরতে পারে।  মুদ্রা বা নোট সহ কোনও ঘন ঘন স্পর্শ করা পৃষ্ঠ বা বস্তু স্পর্শ করার পরে নিয়মিত এবং ভালভাবে আপনার হাত ধুয়ে নিন।  যদি আপনার হাত পরিষ্কার না করা হয় তবে আপনার চোখ, মুখ এবং নাকের স্পর্শ এড়িয়ে চলুন।
