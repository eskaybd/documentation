---
title: 'আমার কি কভিড -১৯ নিয়ে চিন্তিত হওয়া উচিৎ? '
published: true
date: '24-04-2020 02:04'
publish_date: '24-04-2020 02:04'
taxonomy:
    category:
        - COVID-19
        - CORONA
        - 'CORONA VIRUS'
    tag:
        - 'worry about covid 19'
---

COVID-19 সংক্রমণের কারণে অসুস্থতা সাধারণত হালকা হয়, বিশেষত বাচ্চাদের এবং অল্প বয়স্কদের ক্ষেত্রে।  তবে এটি মারাত্মক অসুস্থতার কারণ হতে পারে: এটি ধরা পড়ে এমন প্রতি 5 জনের মধ্যে 1 জনকে হাসপাতালের যত্নের প্রয়োজন হয়।  COVID-19 প্রাদুর্ভাব কীভাবে তাদের এবং তাদের প্রিয়জনকে প্রভাবিত করবে তা নিয়ে চিন্তিত হওয়া খুব স্বাভাবিক।

 আমরা আমাদের, আমাদের প্রিয়জন এবং আমাদের সমাজ কে  সুরক্ষিত করতে আমাদের উদ্বেগকে কর্মে রূপান্তর করতে পারি।  এই ক্রিয়াকলাপগুলির মধ্যে প্রথম এবং সর্বাগ্রে হ'ল নিয়মিত এবং পুঙ্খানুপুঙ্খভাবে হাত ধোওয়া এবং ভাল শ্বাস প্রশ্বাসের হাইজিন।  দ্বিতীয়ত, ভ্রমণ, চলাচল এবং সমাবেশে যে কোনও বিধিনিষেধ আরোপ করা সহ স্থানীয় স্বাস্থ্য কর্তৃপক্ষের পরামর্শ অবহিত রাখুন এবং অনুসরণ করুন।