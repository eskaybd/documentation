---
title: 'অ্যান্টিবায়োটিকগুলি COVID-19 প্রতিরোধ বা চিকিৎসার ক্ষেত্রে কার্যকর? '
published: true
date: '24-04-2020 02:11'
publish_date: '24-04-2020 02:11'
taxonomy:
    category:
        - COVID-19
        - 'CORONA VIRUS'
    tag:
        - 'antibiotics in covid19'
        - 'এন্টিবায়োটিক করোনা চিকিৎসা'
---

না। অ্যান্টিবায়োটিকগুলি ভাইরাসের বিরুদ্ধে কাজ করে না, তারা কেবল ব্যাকটেরিয়া সংক্রমণের জন্য কাজ করে।  COVID-19 একটি ভাইরাস দ্বারা সৃষ্ট, তাই অ্যান্টিবায়োটিক কাজ করে না।  অ্যান্টিবায়োটিকগুলি COVID-19 এর প্রতিরোধ বা চিকিৎসার উপায় হিসাবে ব্যবহার করা উচিত নয়।  এগুলি কেবল ব্যাকটিরিয়া সংক্রমণের জন্য চিকিৎসকের পরামর্শ অনুযায়ী ব্যবহার করা উচিত।
