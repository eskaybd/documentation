---
title: 'করোনভাইরাস COVID-19 এর উৎস কি ?'
taxonomy:
    category:
        - docs
---

বর্তমানে, SARS-CoV-2, করোনা ভাইরাস (CoV) -র কভিড -১৯-এর উৎস অজানা।  সমস্ত প্রমাণগুলি থেকে বোঝা যায় যে সারস-কোভ -২ এর প্রাকৃতিক প্রাণীর উৎস রয়েছে এবং এটি কোনও ল্যাবে তৈরি ভাইরাস নয়।  SARS-CoV-2 ভাইরাস এর বাস্তুসংস্থান রয়েছে বাদুরে।।  সারস-কোভ -২, ভাইরাসগুলি একটি জেনেটিক্যালি গ্রুপের অন্তর্গত, যার মধ্যে SARS-CoV এবং বাদুড় এর ভিতর থেকে বিচ্ছিন্ন বেশ কয়েকটি অন্যান্য CoV রয়েছে। এছাড়াও MERS-CoV এই গ্রুপের অন্তর্গত, তবে কম ঘনিষ্ঠভাবে সম্পর্কিত।