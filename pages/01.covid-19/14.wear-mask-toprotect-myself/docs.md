---
title: 'নিজেকে বাঁচানোর জন্য কি আমার মাস্ক পরা উচিত?'
published: true
date: '24-04-2020 02:38'
publish_date: '24-04-2020 02:38'
taxonomy:
    category:
        - COVID-19
        - কভিড-১৯
        - 'CORONA VIRUS'
    tag:
        - COVID-19
        - 'করোনা ভাইরাস'
        - কভিড-১৯
        - 'করোনা ঝুকি'
---

আপনি যদি COVID-19 উপসর্গগুলি (বিশেষত কাশি) নিয়ে অসুস্থ হন বা কভিড -১৯ আক্রান্ত ব্যাক্তির দেখাশোনা করছেন সে ক্ষেত্রে  কেবলমাত্র একটি মাস্ক পরুন।  ডিসপোজেবল ফেস মাস্ক কেবল একবার ব্যবহার করা যেতে পারে।  আপনি যদি অসুস্থ না হন বা অসুস্থ ব্যক্তির দেখাশোনা না করেন তবে আপনি একটি মাস্ক নষ্ট করছেন।  বিশ্বব্যাপী মাস্ক এর  অভাব রয়েছে, তাই ডাব্লুএইচও (WHO) জনগণকে সঠিকভাবে মাস্ক ব্যবহার করার জন্য অনুরোধ করে।

 ডাব্লুএইচও মূল্যবান সম্পদের অপ্রয়োজনীয় অপচয় এবং মাস্ক অপব্যবহার এড়াতে চিকিৎসার ক্ষেত্রে মাস্ক গুলির যৌক্তিক ব্যবহারের পরামর্শ দেয়

COVID-19 এর থেকে নিজেকে এবং অন্যদের রক্ষার সর্বাধিক কার্যকর উপায় হ'ল ঘন ঘন আপনার হাত পরিষ্কার করা, আপনার কাশিটি কনুই বা টিস্যুর মাধ্যমে মুখ ঢেকে রাখা  এবং কাশি বা হাঁচি করা লোকদের থেকে কমপক্ষে 1 মিটার (3 ফুট) দূরত্ব বজায় রাখা