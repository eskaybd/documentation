---
title: 'COVID-19 এর জন্য কোনও ভ্যাকসিন, ড্রাগ বা চিকিৎসা নেই?'
taxonomy:
    category:
        - COVID-19
        - 'CORONA VIRUS'
    tag:
        - Drugs
        - 'Treatment of COVID-19'
---

এখনো না। আজ অবধি, কোভিড -২০১৯ প্রতিরোধ বা চিকিৎসার জন্য কোনও ভ্যাকসিন এবং নির্দিষ্ট অ্যান্টিভাইরাস ওষুধ নেই।  তবে, ক্ষতিগ্রস্থদের লক্ষণগুলি থেকে মুক্তি দেওয়ার জন্য যত্ন নেওয়া উচিত।  গুরুতর অসুস্থ ব্যক্তিদের হাসপাতালে ভর্তি করা উচিত।  বেশিরভাগ রোগী যত্ন নেয়ার মাধ্যমে সেরে ঊঠেন ।

 সম্ভাব্য ভ্যাকসিনগুলি এবং কিছু নির্দিষ্ট ড্রাগ চিকিৎসা  তদন্তাধীন রয়েছে।  ক্লিনিকাল ট্রায়ালের মাধ্যমে তাদের পরীক্ষা করা হচ্ছে।  ডাব্লুএইচও, কোভিড -১৯ প্রতিরোধ ও চিকিৎসার জন্য ভ্যাকসিন এবং ওষুধ তৈরির বিভিন্ন প্রচেষ্টা সমন্নয় করছে ।

 COVID-19 এর থেকে নিজেকে এবং অন্যদের রক্ষার সর্বাধিক কার্যকর উপায় হ'ল ঘন ঘন আপনার হাত পরিষ্কার করা, হাচ্ছি বা কাশি দেয়ার সময়  কনুই বা টিস্যু দিয়ে নাক মুখ ঢেকে রাখা এবং যিনি  কাশি দিচ্ছেন তার থেকে কমপক্ষে ১ মিটার (৩ ফুট) দূরত্ব বজায় রাখা।  