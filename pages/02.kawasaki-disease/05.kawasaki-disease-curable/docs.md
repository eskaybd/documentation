---
title: 'কাওয়াসাকি রোগ কি নিরাময়যোগ্য?'
published: true
date: '14-06-2020 00:26'
publish_date: '14-06-2020 00:26'
metadata:
    'কাওয়সাকি রোগ': 'কাওয়সাকি কি, কাওয়সাকি কেনো হয়'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি কি'
        - 'কাওয়াসাকি রোগ'
jscomments:
    active: true
    provider: facebook
child_type: default
routable: true
visible: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '13-06-2020 18:27'
    dateModified: '13-06-2020 18:27'
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

উচ্চ জ্বর এবং  ত্বকের খোসা উঠার মতো কাওয়াসাকি রোগের লক্ষণগুলি ভীতিজনক হতে পারে।  সুসংবাদটি হ'ল কাওয়াসাকি রোগটি সাধারণত চিকিতসাযোগ্য এবং বেশিরভাগ শিশু গুরুতর সমস্যা ছাড়াই কাওয়াসাকি রোগ থেকে সেরে ওঠে।