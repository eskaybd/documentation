---
title: 'কাওয়াসাকি রোগের ধাপ গুলি কী কী?'
published: true
date: '14-06-2020 01:57'
publish_date: '14-06-2020 01:57'
metadata:
    'কাওয়াসাকি রোগ': 'কাওয়াসাকি '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকির রোগটি তিনটি পর্যায়ে বিভক্ত করা যেতে পারে: তীব্র, উপ তিব্র এবং কনভ্যালসেন্ট (convalescent)।   তীব্র পর্যায়ে সাধারণত সাত থেকে চৌদ্দ দিন স্থায়ী হয় এবং এটি জ্বর, চোখ ও মুখের পরিবর্তন, ফোলাভাব এবং হাত ও পায়ের লালভাব, ফুসকুড়ি এবং উত্থিত লিম্ফ নোডের দ্বারা চিহ্নিত হয়।