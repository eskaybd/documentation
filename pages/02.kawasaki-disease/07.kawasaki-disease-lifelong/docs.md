---
title: ' কাওয়াসাকি রোগ কি আজীবন?'
published: true
date: '14-06-2020 00:48'
publish_date: '14-06-2020 00:48'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়সাকি রোগ কি, কেনো হয়'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়সাকি রোগ'
        - 'কাওয়াসাকি রোগ'
        - 'কাওয়াসাকি কি'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকির রোগের লক্ষণগুলি সাধারণত এক বা দুই মাসের মধ্যেই সমাধান হয়ে যায় তবে এই রোগটিকে "আজীবন রোগ" হিসাবে বিবেচনা করা উচিত কারণ দেরিতে-শুরু হওয়া হার্টের ধমনী পরিবর্তনের জন্য তদারকি করা জরুরি।  কাওয়াসাকি রোগে আক্রান্ত কিছু শিশু করোনারি ধমনীতে ক্ষত ভোগ করে।