---
title: 'কাওয়াসাকি(Kawasaki) রোগ কি জেনেটিক?'
published: true
date: '14-06-2020 01:32'
publish_date: '14-06-2020 01:32'
metadata:
    'কাওয়াসাকি রোগ  ': 'কাওয়াসাকি জেনেটিক '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'kawasaki genetic'
        - 'কাওয়াসাকি জেনেছে'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগ পরিবারগুলিতে প্রজন্মের মধ্যে দিয়ে গেছে বলে মনে হয়, তবে উত্তরাধিকারের ধারণাটি অজানা। কাওয়াসাকি (Kawasaki) রোগে আক্রান্ত বাবা-মায়েদের বাচ্চাদের মধ্যে সাধারণ জনগণের তুলনায় দ্বিগুণ অসুস্থতা হওয়ার ঝুঁকি থাকে।