---
title: ' কাওয়াসাকি রোগে সবচেয়ে বেশি কারা আক্রান্ত হয় ?'
published: true
date: '14-06-2020 00:57'
publish_date: '14-06-2020 00:57'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি, কাওয়াসাকি কি'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি রোগ'
        - 'কাওয়াসাকি কি'
        - 'কাওয়াসাকি লক্ষন'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকির রোগটি প্রায়শই প্রায় পাঁচ বছর বা তার চেয়ে কম বয়সী বাচ্চারা আক্রান্ত হয়।  