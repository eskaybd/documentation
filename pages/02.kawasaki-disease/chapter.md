---
title: 'কাওয়াসাকি রোগ সম্পর্কিত সাধারন প্রশ্ন উত্তর'
taxonomy:
    category: docs
child_type: docs
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

### Kawasaki Disease
# কাওয়াসাকি রোগ সম্পর্কিত সাধারন প্রশ্ন উত্তর

কাওয়াসাকি রোগ এমন একটি অসুস্থতা যা প্রায়শই অল্প বয়স্ক শিশুদের মধ্যে রক্তনালী গুলিকে প্রদাহে পরিণত করে।  এটি বাচ্চাদের হৃদরোগের অন্যতম প্রধান কারণ।  তবে চিকিতসকরা যদি তাড়াতাড়ি খুঁজে পান তবে এটি চিকিতসা করতে পারেন।  বেশিরভাগ শিশুরা কোনও সমস্যা ছাড়াই সুস্থ হয়ে উঠেন।
!!!! কাওয়াসাকি সম্পর্কে আরো জানার জন্য-[কাওয়াসাকি রোগ কি? কাওয়াসাকি রোগ সম্পর্কে যা জানা দরকার।](https://al-amin.info/blog/kawasaki) পোস্ট পড়ে আসতে পারেন।