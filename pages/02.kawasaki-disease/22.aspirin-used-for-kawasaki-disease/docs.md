---
title: 'কাওয়াসাকি রোগের জন্য কেন অ্যাসপিরিন ব্যবহার করা হয়?'
published: true
date: '14-06-2020 02:01'
publish_date: '14-06-2020 02:01'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাক,   '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
        - অ্যাসপিরিন
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

এটি রেয়েস সিনড্রোম সহ(Reye's syndrome) পার্শ্ব প্রতিক্রিয়া সৃষ্টি করতে পারে।  অ্যাসপিরিন একটি অ-স্টেরয়েডাল অ্যান্টি-ইনফ্লেমেটরি ড্রাগ (এনএসএআইডি)(non-steroidal anti-inflammatory drug (NSAID))।  এটি কাওয়াসাকির রোগের চিকিতসার জন্য ব্যবহৃত হয় কারণ: ... কম মাত্রায় অ্যাসপিরিন একটি প্রতিষেধক (এটি রক্তের জমাট বাঁধা প্রতিরোধ করে)