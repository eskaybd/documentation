---
title: 'কাওয়াসাকি রোগ কি?'
published: true
date: '14-06-2020 00:15'
publish_date: '14-06-2020 00:15'
metadata:
    'কাওয়াসাকি রোগ': 'কাওয়াসাকি, কাওয়াসাকি কই'
taxonomy:
    category:
        - docs
        - কাওয়াসাকি
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি কি'
        - 'কাওয়াসাকি রোগ'
jscomments:
    active: true
    provider: facebook
routable: true
cache_enable: true
twittercardoptions: summary
articleenabled: false
article:
    datePublished: '13-06-2020 18:14'
    dateModified: '13-06-2020 18:14'
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগ (কেডি), যাকে কাওসাকী সিন্ড্রোম হিসাবেও পরিচিত, এটি একটি তীব্র ফীব্রিল ডিজিজ  যা অজানা কারণে হয় এবং প্রাথমিকভাবে ৫ বছরের কম বয়সের বাচ্চাদের আক্রান্ত করে থাকে।  ১৯৬৭ সালে টোমিসাকু কাওয়াসাকি (Tomisaku Kawasaki) এই রোগটি প্রথম জাপানে বর্ণনা করেছিলেন এবং ১৯৭৬ সালে জাপানের বাইরে প্রথম ঘটনাটি  প্রকাশিত হয়েছিল হাওয়াইতে (Hawaii)।  ক্লিনিকাল লক্ষণগুলির মধ্যে রয়েছে জ্বর, ফুসকুড়ি, হাত ও পা ফোলাভাব, চোখের সাদা অংশে জ্বালা এবং লালভাব, ঘাড়ে লসিকা গ্রন্থি ফোলা এবং মুখ, ঠোঁটে গলায় জ্বালা করা।  


কাওয়াসাকি রোগ এমন একটি অসুস্থতা যা সারা দেহে রক্তনালীতে প্রদাহ (ফোলাভাব এবং লালভাব) সৃষ্টি করে।  এটি তিন ধাপে ঘটে এবং স্থায়ী জ্বর সাধারণত প্রথম লক্ষণ।  এই অবস্থাটি প্রায়শই ৫ বছরের কম বয়সী বাচ্চাদের ক্ষেত্রে ঘটে করে।