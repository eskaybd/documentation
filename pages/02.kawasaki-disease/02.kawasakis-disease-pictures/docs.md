---
title: ' কাওয়াসাকির রোগের লক্ষন কী?'
published: true
date: '14-06-2020 01:04'
publish_date: '14-06-2020 01:04'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি লক্ষন'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়সাকি লক্ষন'
        - 'কাওয়াসাকি লক্ষন'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

কাওসাকির এই রোগটি সন্দেহ করা হয় তখনি যখন একটি শিশুর ৫ দিনেরও বেশি সময় ধরে জ্বর হয় এবং নিম্নলিখিত নীচের কয়েকটি লক্ষণ দেখায়: ফুসকুড়ি, ঘাড়ের লিম্ফ নোডগুলি ফুলে যায় (neck lymph nodes swelling), লাল কনজেন্টিভা, স্ট্রবেরি জিহ্বা এবং ত্বকের খোসা ছাড়ায়।