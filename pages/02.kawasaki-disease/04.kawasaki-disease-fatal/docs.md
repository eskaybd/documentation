---
title: 'কাওয়াসাকি রোগ কি মারাত্মক?'
published: true
date: '14-06-2020 00:36'
publish_date: '14-06-2020 00:36'
metadata:
    'কাওয়সাকি রোগ': 'কাওয়সাকি কি, কাওয়সাকি কেনো হয়'
taxonomy:
    category:
        - docs
        - 'কাওয়সাকি রোগ'
    tag:
        - 'কাওয়সাকি কি'
        - 'কাওয়সাকি এর লক্ষন'
        - 'কাওয়সাকির তিব্রতা'
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

আর্থ্রাইটিস ফাউন্ডেশন নোট করে যে, প্রতি ৪ জনের মধ্যে ১ জন শিশু যথাযথ চিকিতসা করেও তাদের করোনারি ধমনীতে সমস্যা তৈরি করতে পারে এবং কাওয়াসাকি রোগ প্রায় ১ শতাংশ ক্ষেত্রে মারাত্মক।  চিকিতসা না করা অবস্থায়, কাওসাকি রোগ অ্যানিউরিজম(aneurysm)  সহ গুরুতর জটিলতা সৃষ্টি করতে পারে