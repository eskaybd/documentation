---
title: 'কাওয়াসাকি রোগটি কি একটি অটোইমিউন ডিসঅর্ডার?'
published: true
date: '14-06-2020 00:40'
publish_date: '14-06-2020 00:40'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়সাকি রোগ কি '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
        - 'কাওয়াসাকি কেনো হয়'
        - 'কাওয়াসাকি লক্ষন'
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

কাওয়াসাকি রোগটি ভালভাবে বোঝা যায় না এবং এর কারণ এখনও অজানা।  এটি অটোইমিউন ডিসঅর্ডার হতে পারে।  সমস্যাটি শ্লেষ্মা ঝিল্লি, লিম্ফ নোডগুলি, রক্তনালীগুলির দেয়াল এবং হার্টকে প্রভাবিত করে।