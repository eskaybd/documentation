---
title: 'প্রাপ্তবয়স্করা কি কাওয়াসাকি রোগে আক্রান্ত হতে পারে ? '
published: true
date: '14-06-2020 01:15'
publish_date: '14-06-2020 01:15'
metadata:
    'কাওয়াসাকি রোগ  ': 'কাওয়াসাকি প্রাপ্তবয়স্ক  '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি প্রাপ্তবয়স্ক'
        - কাওয়াসাকি
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকির রোগ, বা মিউকুচেনিয়াস লিম্ফ নোড সিনড্রোম প্রধানত বাচ্চাদের মধ্যে এবং প্রাপ্তবয়স্কদের মধ্যে খুব কমই ঘটে।  লক্ষণগুলির মধ্যে রয়েছে তীব্র ভাস্কুলাইটিস, শ্লেষ্মা প্রদাহ, ফুসকুড়ি, জরায়ুর অ্যাডিনোপ্যাথি, হাত ও পা ফোলা এবং দেরিতে আঙ্গুলের ডেস্কামেশন 