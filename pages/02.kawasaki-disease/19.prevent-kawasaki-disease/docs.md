---
title: 'কীভাবে আপনি কাওয়াসাকি রোগ প্রতিরোধ করবেন?'
published: true
date: '14-06-2020 01:49'
publish_date: '14-06-2020 01:49'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি প্রতিরোধ'
        - কাওয়াসাকি
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

উচ্চ ডোজের অ্যাসপিরিন গ্রহনকারি বাচ্চাদের পক্ষে এই ভাইরাসজনিত অসুস্থতা রোধে বার্ষিক ফ্লু ভ্যাকসিন পাওয়া খুব জরুরি।  এর কারণ এমন যে শিশুরা ভাইরাল অসুস্থতার সময় অ্যাসপিরিন গ্রহণ করে তাদের মধ্যে রেই সিনড্রোম নামে একটি বিরল অবস্থার একটি ছোট ঝুঁকি রয়েছে।