---
title: 'আপনার সন্তান কিভাবে কাওয়াসাকি রোগে আক্রান্ত হতে পারে ?'
published: true
date: '14-06-2020 00:23'
publish_date: '14-06-2020 00:23'
taxonomy:
    category:
        - docs
        - 'কাওয়সাকি রোগ'
    tag:
        - কাওয়াসাকি
        - 'কাওয়সাকি রোগ'
        - 'কাওয়াসাকি লক্ষন'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

এটি লিম্ফ নোডস (lymph nodes), ত্বক এবং কোনও শিশুর মুখ, নাক এবং গলার আস্তরণের সমস্যা তৈরি করতে পারে।  বিজ্ঞানীরা কাওয়াসাকি রোগের সঠিক কারণ খুঁজে পাননি।  এটি জিন, ভাইরাস, ব্যাকটেরিয়া এবং কোনও শিশুর আশেপাশের বিশ্বের অন্যান্য জিনিসের সাথে যুক্ত হতে পারে যেমন রাসায়নিক এবং জ্বালা।