---
title: ' কাওয়াসাকি রোগ কি হাত পা ও মুখের রোগের মতো?'
published: true
date: '14-06-2020 01:12'
publish_date: '14-06-2020 01:12'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি লক্ষন'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

ফুসকুড়ি নিজেই সংক্রামক নয়।  ভাইরাসটি ছোট ছোট ফোস্কা এবং তালুতে, পায়ের ত্বকে এবং মুখের অভ্যন্তরে লাল দাগের কারণ হতে পারে ... এই অবস্থাটি হাত-পা-মুখের রোগ (এইচএফএমডি) নামে পরিচিত এবং গ্রুপ A কক্সস্যাকিভাইরাস দ্বারা সংঘটিত হয়েছিল।  এইচএফএমডি ১০ বছরের কম বয়সী বাচ্চাদের মধ্যে সবচেয়ে বেশি দেখা যায়।