---
title: 'কাওয়াসাকি রোগ কোন অঙ্গগুলিকে ক্ষতিগ্রস্ত করে?'
published: true
date: '14-06-2020 01:21'
publish_date: '14-06-2020 01:21'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি রোগের ক্ষতি'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি রোগ'
        - 'কাওয়াসাকি রোগের ক্ষতি'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

এটি হৃদপিণ্ডের রক্তনালীগুলি (করোনারি ধমনী) সহ পুরো শরীরকে প্রভাবিত করতে পারে।  কাওয়াসাকি রোগের কারণ অজানা।  চিকিতসা না করে আক্রান্ত শিশুরা করোনারি ধমনীতে সমস্যা বৃদ্ধির ঝুঁকিতে বেশি।  হার্টের অন্যান্য ক্ষেত্রগুলিও আক্রান্ত হতে পারে।