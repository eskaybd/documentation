---
title: 'কাওয়াসাকি কি পরিবারের মধ্যে চলমান?'
published: true
date: '14-06-2020 01:46'
publish_date: '14-06-2020 01:46'
metadata:
    'কাওয়াসাকি রোগ  ': 'কাওয়াসাকি  '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগের একটি পূর্বাভাস পরিবারের প্রজন্মের মাধ্যমে স্থানান্তরিত হয়েছে, তবে উত্তরাধিকার সুত্রে এটি অজানা। কাওয়াসাকি রোগে আক্রান্ত বাবা-মায়ের সন্তানরা সাধারণ জনগোষ্ঠীর চেয়ে ঝুঁকি বেশি।