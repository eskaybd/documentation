---
title: 'কাওয়াসাকি রোগ থেকে সেরে উঠতে কত সময় লাগে?'
published: true
date: '14-06-2020 01:29'
publish_date: '14-06-2020 01:29'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি রোগ'
        - kawasaki
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগ শুরু হওয়ার তিন-চার সপ্তাহ না হওয়া পর্যন্ত ত্বকের খোসা ছাড়ানোর মতো কিছু লক্ষণ দেখা যায় না এবং পুরো সুস্থ হতে প্রায় ছয় সপ্তাহ লাগতে পারে।