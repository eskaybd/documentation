---
title: 'কাওয়াসাকি রোগ কীভাবে ছড়ায়?'
published: true
date: '14-06-2020 01:54'
publish_date: '14-06-2020 01:54'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি ছড়ায়'
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগ সংক্রামক নয়।  এটি পরিবারের সদস্য বা শিশু যত্ন কেন্দ্রগুলিতে বাচ্চাদের মধ্যে ছড়িয়ে যায় না ... কাওয়াসাকি রোগের কারণ জানা যায় নি, তবে এটি শরীরের রোগ প্রতিরোধ ব্যবস্থা দ্বারা সৃষ্ট  প্রতিক্রিয়া বলে মনে করা হয় ।