---
title: 'কাওয়াসাকি রোগের জন্য দায়ি কে ?'
published: true
date: '14-06-2020 00:53'
publish_date: '14-06-2020 00:53'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়সাকি রোগ কি, রোগ'
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - কাওয়াসাকি
        - 'কাওয়াসাকি লক্ষন'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

এটি লিম্ফ নোডল্যম্ফ(lymph nodes), ত্বক এবং মুখ, নাক এবং গলার আস্তরণের সমস্যা তৈরি করতে পারে।  বিজ্ঞানীরা কাওয়াসাকি রোগের সঠিক কারণ খুঁজে পান নি।  তবে তারা মনে করেন এটি সম্ভবত জিনেটিক্স, ভাইরাস এবং ব্যাকটেরিয়াগুলির সংস্পর্শ এবং অন্যান্য পরিবেশগত কারণগুলি যেমন রাসায়নিক এবং জ্বালা সম্পর্কিত সংযুক্তির সাথে যুক্ত।