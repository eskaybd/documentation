---
title: 'কাওয়াসাকি রোগের চিকিতসা'
published: true
date: '14-06-2020 01:06'
publish_date: '14-06-2020 01:06'
metadata:
    'কাওয়াসাকি রোগ': 'কাওয়াসাকি চিকিৎসা '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি চিকিৎসা'
        - 'কাওয়াসাকি চিকিতসা'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
facebookauthor: 'https://facebook.com/Eskaybd'
---

জ্বর, ফোলাভাব এবং ত্বকের সমস্যা থেকে আপনার সন্তানের প্রচুর ব্যথা হতে পারে।  তাদের চিকিতসক তাদের আরও ভাল বোধ করার জন্য ওষুধগুলি লিখে দিতে পারে যেমন এসপিরিন এবং সেই সব ওষুধ যা রক্ত ​​জমাট বাঁধা প্রতিরোধ করে।