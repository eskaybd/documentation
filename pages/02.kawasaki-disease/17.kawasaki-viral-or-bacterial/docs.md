---
title: 'কাওয়াসাকি রোগ কি ভাইরাল বা ব্যাকটেরিয়া জনিত? '
published: true
date: '14-06-2020 01:42'
publish_date: '14-06-2020 01:42'
metadata:
    'কাওয়াসাকি রোগ ': 'কাওয়াসাকি,  ভাইরা,  ব্যক্টেরিয়ার   '
taxonomy:
    category:
        - docs
        - 'কাওয়াসাকি রোগ'
    tag:
        - 'কাওয়াসাকি ব্যাকটেরিয়া'
        - 'কাওয়াসাকি ভাইরাস'
cache_enable: true
visible: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookauthor: 'https://facebook.com/Eskaybd'
---

কাওয়াসাকি রোগের লক্ষণগুলি সংক্রমণের মতো, তাই ব্যাকটেরিয়া বা ভাইরাস দায়ী হতে পারে। কিন্তু  এটি ব্যাকটেরিয়া বা ভাইরাল কারণ জনিত কিনা তা চিহ্নিত করা হয় নি। কাওয়াসাকি রোগ সংক্রামক নয়, এটি একজন ব্যক্তির কাছ থেকে অন্যের ব্যক্তির কাছে সংক্রমণ ঘটায় না।
